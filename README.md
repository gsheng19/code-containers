hihi AASP Code Containers
===============  

These are the code containers that (may)/are currently used in the AASP Code Compiler backend worker. 


## Overview
This is a subset of a forked project from glot-containers, which
are a collection of docker images that are used
by [glot.io](https://glot.io) to run code. The built images can be
found on [the container registry](https://gitlab.com/kennethsohyq/school/university/fyp/code-containers/container_registry).

