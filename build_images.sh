#!/bin/bash
shopt -s globstar
set -e

x64_BINARY=https://gitlab.com/kennethsohyq/school/university/fyp/code-runner/-/package_files/6365068/download

for dockerfile in **/Dockerfile; do
    tagPath=$(dirname "$dockerfile")
    imagePath=$(dirname "$tagPath")
    tag=$(basename "$tagPath")
    image=$(basename "$imagePath")
    imageName="registry.gitlab.com/kennethsohyq/school/university/fyp/code-containers/${image}:${tag}"

    # Build image
    (
        echo "Building $imageName"
        cd "$tagPath"
        docker build --pull --no-cache --network host -t "$imageName" --build-arg downloadurl=$x64_BINARY . || true
    )
done
