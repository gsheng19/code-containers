#!/bin/bash
shopt -s globstar
set -e

# We shall do this instead as due to the lack of space on the ci instance, we cannot build everything THEN push sadly

# Required arg
foldertogo=$1

echo "Building for $foldertogo"
cd "$foldertogo"

for dockerfile in **/Dockerfile; do
    (
        tag=$(dirname "$dockerfile")
        imagePath=$(basename "$PWD")
        image=$(basename "$imagePath")
        imageName="registry.gitlab.com/gsheng19/code-containers/${image}:${tag}"
    
        # Build image
        echo "Building $imageName"
        cd "$tag"
        if [[ -z "${CODE_CONTAINER_MULTIARCH}" ]]; then
            echo "~~~Building Single Architecture (x64/amd64) Container~~~"
            docker build --pull --network host -t "$imageName" --build-arg downloadurl=$x64_BINARY . || true

            # Push image
            echo
            echo "Pushing $imageName"
            docker push "$imageName"
        else
            echo "~~~Building Multi-Architecture (arm) Container~~~"
            docker buildx build --platform linux/arm/v7 --progress=plain --push -t "$imageName-armv7" --build-arg downloadurl=$ARM_BINARY . && armv7res=1 || armv7res=0
            docker buildx build --platform linux/arm/v6 --progress=plain --push -t "$imageName-armv6" --build-arg downloadurl=$ARM_BINARY . && armv6res=1 || armv6res=0
            echo "~~~Building Multi-Architecture (arm64) Container~~~"
            docker buildx build --platform linux/arm64 --progress=plain --push -t "$imageName-arm64" --build-arg downloadurl=$ARM64_BINARY . && arm64res=1 || arm64res=0
            echo "~~~Building Multi-Architecture (x64/amd64) Container~~~"
            docker buildx build --platform linux/amd64 --progress=plain --push -t "$imageName-x64" --build-arg downloadurl=$x64_BINARY . && x64res=1 || x64res=0

            echo "~~~Detecting what build worked~~~"
            s=""

            if [[ $armv6res -eq 1 ]]; then
                echo "~~~Appending armv6~~~"
                s+=" $imageName-armv6"
            fi
            
            if [[ $armv7res -eq 1 ]]; then
                echo "~~~Appending armv7~~~"
                s+=" $imageName-armv7"
            fi

            if [[ $arm64res -eq 1 ]]; then
                echo "~~~Appending arm64~~~"
                s+=" $imageName-arm64"
            fi

            if [[ $x64res -eq 1 ]]; then
                echo "~~~Appending x64/amd64~~~"
                s+=" $imageName-x64" 
            fi


            echo "~~~Merging manifest~~~"
            docker buildx imagetools create -t "$imageName" $s
            docker buildx imagetools inspect "$imageName"
        fi
    )
done
